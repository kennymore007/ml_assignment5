#!/usr/bin/env python
# Sources used include
# https://stackabuse.com/association-rule-mining-via-apriori-algorithm-in-python/
# from where dataset is sourced dataset_url(https://drive.google.com/file/d/1y5DYn0dGoSbC22xowBq2d4po6h1JxcTQ/view)
# and https://www.youtube.com/watch?v=SVM_pX0oTU8


from matplotlib.pyplot import xticks
import common.test_env as test_env
import common.describe_data as describe_data
import pandas as pd
import numpy as np
from pandas import read_csv
from apyori import apriori
from matplotlib import pyplot as plt


def read_data(file):
    """Return pandas dataFrame read from csv file"""
    try:
        return pd.read_csv(file, sep=';', decimal=',', encoding='latin-1')
    except FileNotFoundError:
        exit('ERROR: ' + file + ' not found')


def preprocess_data(total_records):
    """Apriori requires our data to be in the form of list of lists from pandas dataframe"""
    records = []
    for i in range(0, total_records):
        records.append([str(store_data.values[i, j]) for j in range(0, 20)])
    return records


def store_suggestion_result(records):
    association_rules = apriori(
        records, min_support=0.0053, min_confidence=0.2, min_lift=3, min_length=2)
    # builds model and converts to list
    association_results = list(association_rules)

    print(len(association_results))
    print(association_results[0])

    results = []
    for item in association_results:
        pair = item[0]
        items = [x for x in pair]

        value0 = str(items[0])
        value1 = str(items[1])
        value2 = str(item[1])[:7]
        value3 = str(item[2][0][2])[:7]
        value4 = str(item[2][0][3])[:7]

        rows = (value0, value1, value2, value3, value4)

        results.append(rows)

        Label = ['Title1', 'Title2', 'Support', 'Confidence', 'Lift']

        store_suggestions = pd.DataFrame.from_records(results, columns=Label)

        describe_data.custom_print(
            store_suggestions, file='results/store_suggestion_data.txt')


if __name__ == '__main__':
    REQUIRED = ['matplotlib', 'pandas']
    test_env.versions(REQUIRED)
    store_data = read_csv('data/store_data.csv',  delimiter=',', header=None)

    describe_data.print_overview(
        store_data, file='results/store_overview.txt')
    describe_data.print_categorical(
        store_data, file='results/store_categorical_data.txt')
    num_records = len(store_data)
    records = preprocess_data(num_records)
    store_suggestion_result(records)
