#!/usr/bin/env python
from matplotlib.pyplot import xticks
import common.test_env as test_env
import common.describe_data as describe_data
import pandas as pd
import numpy as np
from pandas import read_csv
from apyori import apriori
from matplotlib import pyplot as plt


def data_pre_process(df):
    df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
    df = df.fillna(0)
    return df


if __name__ == '__main__':
    REQUIRED = ['matplotlib', 'pandas']
    test_env.versions(REQUIRED)
    read_file = pd.read_excel(r'data/demoAssoc.xls')
    read_file.to_csv(
        r'data/store.csv',
        index=False,
        header='None',
        encoding='utf-8')
    store_data = read_csv('data/store.csv', index_col=0)
    describe_data.print_overview(
        store_data, file='results/market_overview.txt')
    describe_data.print_categorical(store_data,
                                    file='results/categorical_data_market.txt')
    processed_df = data_pre_process(store_data)
    print(processed_df)
    # print statement just helps you see the data after preprocessing can
    # remove when everything is ok
